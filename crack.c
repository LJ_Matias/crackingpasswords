#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password will be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a target hash, crack it. Return the matching
// password.
char * crackHash(char *target, char *dictionary)
{
    // Open the dictionary file
    FILE *file = fopen( dictionary, "r" );
    if ( !file ){
        printf("Unable to open %s\n", dictionary);
        exit(1);
    }

    // Loop through the dictionary file, one line
    // at a time.
    char line[PASS_LEN];
    while (fgets(line, PASS_LEN, file) != NULL){
        // Removes \n from input (comes from fgets)
        char *nl = strchr(line, '\n');
        if (nl) *nl = '\0';

        char *password = malloc(PASS_LEN);
        password = line;

        // Hash each password. Compare to the target hash.
        // If they match, return the corresponding password.
        if ( strcmp( md5( line, strlen(line) ), target) == 0 ){
            fclose( file );
            return password;
        }
    }
    fclose( file );

    return NULL;
}

int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Open the hash file for reading.
    FILE *hashFile = fopen(argv[1], "r");
    if ( !hashFile ){
        printf("Unable to open %s\n", argv[1]);
        exit(1);
    }

    // For each hash, crack it by passing it to crackHash
    char input[HASH_LEN];
    while (fgets(input, HASH_LEN+1, hashFile) != NULL){
        // Removes \n from input (comes from fgets)
        char *nl = strchr(input, '\n');
        if (nl) *nl = '\0';

        char *crack = malloc(PASS_LEN);
        strcpy(crack, crackHash(input, argv[2]));

        if (crack != NULL){
            // Display the hash along with the cracked password:
            //   5d41402abc4b2a76b9719d911017c592 hello
            printf("%s :%s:\n", input, crack);
        }
        // Free up any malloc'd memory?
        free(crack);
    }
    // Close the hash file
    fclose(hashFile);
    return(0);
}
